.PHONY: format package install release pypi pypitest

format:
	black -l 79 pensum/*.py

package:
	python3 setup.py sdist bdist_wheel

install: package
	sudo pip3 install dist/pensum-2022.3.14-py3-none-any.whl --force-reinstall

release:
	-cp dist/pensum-0.1-py3-none-any.whl downloads/

pypitest:
	python3 -m twine upload --repository testpypi dist/*

pypi:
	python3 -m twine upload --repository pypi dist/*
