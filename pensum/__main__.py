#!/usr/bin/python3
# -*- coding:Utf-8 -*-

"""
Entry point for Pensum.

Copyright (C) 2022  Etienne Nadji

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License along
with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

# Imports ===============================================================#

import pensum.pm

# Variables globales ====================================================#

__author__ = "Etienne Nadji <etnadji@eml.cc>"

# Programme =============================================================#

if __name__ == "__main__":
    pensum.pm.main()

# vim:set shiftwidth=4 softtabstop=4:
