######
Pensum
######

Command lines reminder as a command line software.

Remind notes are written in Markdown and can be exported to any format Pandoc 
handles.

Usage
=====

.. code-block::

    pm ls [<tag>]
    pm cat <note_id> [-d] [-s] [-t]
    pm new <note_id> [<note_title>]
    pm find <request>
    pm build <format> [<output_folder>] [--verbose]
    pm option (set|get) [<option_name>] [<option_value>] [--verbose]
    pm help [<topic>]

* Notes and configuration are stored according to OS conventions (see `folders` help topic).
* For more help about `pm` command, see … `pm` help topic.

Prerequisites
=============

* `docopt <https://github.com/docopt>`_
* `appdirs <https://pypi.org/project/appdirs/>`_
* `rich <https://pypi.org/project/rich/>`_
* `pypandoc <https://pypi.org/project/pypandoc/>`_
* `stop-words <https://pypi.org/project/stop-words/>`_

