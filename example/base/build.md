# Build Pensum notes

How to build Pensum notes in HTML and other formats.

## ID

build

## Tags

pensum, build

## Command

```
psm build html mynotes/
```

## Discussion

- `mynotes/` is the output folder for your “compiled” notes.
- `html` is the output format

For more information during the building process, add `--verbose`.

To list available build formats, use the `help` command :

```
$ pm help buildformats
```
